import sys
from itertools import zip_longest
ver1, ver2 = sys.argv[1], sys.argv[2]
if ver1 == ver2:
    print(0)
else:
    ver1 = map(int, ver1.split("."))
    ver2 = map(int, ver2.split("."))
    for i, j in zip_longest(ver1, ver2, fillvalue=0):
        if i > j:
            print(1)
            break
        elif i < j:
            print(-1)
            break