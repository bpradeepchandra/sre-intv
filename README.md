## Description
This ReadMe file primarily talks about couple of use cases and covers about the repo file along with their importance and details out the approach to carry out the test cases to validate all the scenarios.


## Utility Compare Version:
A utility has been implemented using Python which takes 2 arguments and compares with each other and gives the output in the following format.
If version1 > version2 return 1
If version1 < version2 return -1
If version1 and version2 are equal then it returns 0
The 'dot' character is used to separate number sequences and below is an example of ordering.
Here is an example of version numbers ordering: 0.1 < 1.1 < 1.2 < 1.2.9.9.9.9 < 1.3 < 1.3.4 < 1.10

Based on the above example, below are the list of test cases to can be executed to retrieve the expected output. Assuming that python is locally installed already.
Test cases: Execute following test cases and review the output
1. python UtilVersionCompare.py 1 2  
   Output: -1
2. python UtilVersionCompare.py 2 1  
   Output: 1
3. python UtilVersionCompare.py 1 1  
   Output: 0
4. python UtilVersionCompare.py 1.1 1  
   Output: 1
5. python UtilVersionCompare.py 1.1 1.2  
   Output: -1
6. python UtilVersionCompare.py 1.1.1.1.1.8 1.1.1.1.1.7  
   Output: 1
7. python UtilVersionCompare.py 1.1.1.1.1.7 1.1.1.1.1.8  
   Output: -1
8. python UtilVersionCompare.py 0.0.0.0.0.0.0.1 0.0.0.0.0.0.0.1  
   Output: 0

## Ngnix deployment manifest:
Kubernetes ngnix deployment manifest setup with below configuration:that has each of the following elements.
ConfigMap setup simple key value pair that sets ENV to PROD
Setup Minimun replicas as 3
Nginx pod to listens on port 80
Environment variable setup to read the configmap value
Service to listen on port 80 and balances to all replicas of the above deployment
Setup HPA(Horizontal Pod autoscaler) that will scale up if CPU usage is above 50%

Based on the above use case, here are the test cases to execute and validate if everything is working as expected. Assuming that all required softwares like Git, Kubectl along with Kubernetes cluster setup already exists.
Checkout the git project 
1. git clone -b main https://gitlab.com/bpradeepchandra/test.git  
   Output: Make sure that all files were successfully checked out locally.
2. Now goto current directory through as below  
   cd <Current_Directory> and execute dir for windows and ls for MAC  
   Expected Output: Should see the nginx-deployment-manifest.yaml file exists.  
3. Execute below commands and validate the output.  
   kubectl create -f nginx-deployment-manifest.yaml  
   Expected Output: It should state that all below are created.  
   deployment.apps/nginx created   
   configmap/nginx-config-map created  
   service/nginx-service created  
   horizontalpodautoscaler.autoscaling/nginx-hpa created  
4. Lets check the PODs status now and make sure that it listen on port 80 (It may take a few seconds sometimes the status to change from ContainerCreating to Running state, so please be patient)  
   kubectl get pods  
   Expected Output: Since, in the replicas it was give min as 3 here you should notice 3 runnings ngnix PODs.  
5. Check the Config map by running below commands  
   kubectl get configmap   
   Output: you should see be able to see configmap with name "nginx-config-map"  
   Now, lets describe the configmap using below command to see if the ENV value is set to PROD  
   kubectl describe configmap nginx-config-map  
   Now, lets check if this configmap value is listed as environmental varibale in the pod  
   kubectl exec nginx-54f8cbf95b-f2tnz -- printenv | grep ENV  
   Output: You should be able to see ENV = PROD  
6. Check if nginx Service listen on Port 80 by executing below command  
   kubectl get service  
   Output: Check if its shows listening on TCP with port 80.  
7. Check if the HPA is setup correctly for ngnix deployment by executing below commands  
   kubectl get hpa  
   Output: It shows the HPA setup along with max, min replicas and CPU threshold setup.   
8. Check if ngnix is up and running using below curl command  
    curl http://<POD_IP>:80 | grep "Welcome to nginx!" and it should contain text as "Welcome to nginx!"
